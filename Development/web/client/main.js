import angular from 'angular';
import angularMeteor from 'angular-meteor';

import clients from '../imports/components/clients/clients';
 
angular.module('chefe', [
  angularMeteor,
  clients.name
]);


Router.configure({
 noRoutesTemplate: 'noRoutesTemplate',
});