import { Mongo } from 'meteor/mongo';
import SimpleSchema from 'simpl-schema';

import { OrderItems } from './orderitems.js';
import { MenuItems } from './menuitems.js';

SimpleSchema.extendOptions(['autoform']);

export const Orders = new Mongo.Collection('orders');

Orders.allow({
	insert: () => {
		return true;
	},
	update: () => {
		return true;
	},
	remove: () => {
		return true;
	}
});

Orders.helpers({
	items() {
		let orderItems = OrderItems.find({order:this._id}).fetch();
		for(orderItem of orderItems){
			orderItem.menuitem = MenuItems.findOne({_id:orderItem.menuitem});
		}
		return orderItems;
	}
});

OrdersSchema = new SimpleSchema({
	status:{
		type: String,
		label: 'Status',
		autoform:{
			options: ['Ongoing','Paid','Cancelled']
		}
	},
	date: {
		type: Date,
		label: 'Order Date',
		autoform:{
			type: 'hidden'
		}
	},
	subtotal:{
		type:Number,
		label: 'Subtotal',
		min:0,
		max:99999,
		autoform:{
			placeholder: 'Subtotal'
		}
	},
	total:{
		type: Number,
		label: 'Total',
		min:0,
		max:99999,
		autoform:{
			placeholder: 'Total'
		}
	}
});

OrdersSchema.messageBox.messages({
	es:{
		required: '{{{label}}} es requerido.',
		expectedType: 'valor no compatible para {{{label}}}',
		minNumber: '{{{label}}} debe ser de 0 números.',
		maxNumber: '{{{label}}} no debe exceder 6 números.',
	}
});

OrdersSchema.messageBox.setLanguage('es');

Orders.attachSchema(OrdersSchema);