import { Mongo } from 'meteor/mongo';
import SimpleSchema from 'simpl-schema';

import { MenuItemsImages } from './menuitemsimages.js';

SimpleSchema.extendOptions(['autoform']);

export const MenuItems = new Mongo.Collection('menuitems');

MenuItems.allow({
	insert: () => {
		return true;
	},
	update: () => {
		return true;
	},
	remove: () => {
		return true;
	}
});

MenuItems.helpers({
	images() { 
		return MenuItemsImages.findOne({_id:this.image});
	}
});

MenuItemsSchema = new SimpleSchema({
	status:{
		type: String,
		label: 'Status',
		autoform:{
			options: ['Active','Inactive']
		}
	},
	name:{
		type: String,
		label: 'Name',
		autoform:{
			placeholder: 'Name'
		}
	},
	description:{
		type: String,
		label: 'Description',
		autoform:{
			placeholder: 'Description'
		}
	},
	image:{
		type: String,
		label: 'Item Image',
		optional: true
	},
	price:{
		type:Number,
		label: 'Price',
		min:0,
		max:99999,
		autoform:{
			placeholder: 'Price'
		}
	},
	menu:{
		type: String,
		label: 'Menu'
	},
	createdAt: {
		type:Date,
		label: 'createdAt',
		optional: true,
		autoValue: () => new Date(),
		autoform:{
			type:"hidden"
		}
	}
});

MenuItemsSchema.messageBox.messages({
	es:{
		required: '{{{label}}} es requerido.',
		expectedType: 'valor no compatible para {{{label}}}',
		minNumber: '{{{label}}} debe ser de 0 números.',
		maxNumber: '{{{label}}} no debe exceder 6 números.',
	}
});

MenuItemsSchema.messageBox.setLanguage('es');

MenuItems.attachSchema(MenuItemsSchema);