import { Mongo } from 'meteor/mongo';
import SimpleSchema from 'simpl-schema';

import { Menus } from './menus.js';
import { MenuImages } from './menuimages.js';

SimpleSchema.extendOptions(['autoform']);

export const Clients = new Mongo.Collection('clients');

Clients.allow({
	insert: () => {
		return true;
	},
	update: () => {
		return true;
	},
	remove: () => {
		return true;
	}
});

Clients.helpers({
	menus() {
		let menus = Menus.find({client:this._id}).fetch();
		for(menu of menus){
			if(menu.image!=undefined)
				menu.image = MenuImages.findOne({_id:menu.image}).fetch();
		}
		return menus;
	}
});

ClientsSchema = new SimpleSchema({
	status:{
		type: String,
		label: 'Status',
		autoValue: 'Active',
		autoform:{
			options: ['Active','Suspended']
		}
	},
	name:{
		type: String,
		label: 'Name',
		autoform:{
			placeholder: 'Name'
		}
	},
	l_name:{
		type: String,
		label: 'Last Name',
		autoform:{
			placeholder: 'Last Name'
		}
	},
	dob:{
		type: Date,
		label: 'DOB',
		autoform:{
			placeholder: 'Day of Birth'
		}
	},
	address:{
		type: String,
		label: 'Address',
		autoform:{
			placeholder: 'Address'
		}
	},
	city:{
		type: String,
		label: 'City',
		autoform:{
			placeholder: 'City'
		}
	},
	state:{
		type: String,
		label: 'State',
		autoform:{
			placeholder: 'State'
		}
	},
	zip:{
		type:Number,
		label: 'Zip Code',
		min:9999,
		max:99999,
		autoform:{
			placeholder: 'Zip Code'
		}
	},
	phone:{
		type: Number,
		label: 'Phone',
		min:999999999,
		max:9999999999,
		autoform:{
			placeholder: 'Phone'
		}
	},
	email:{
		type: String,
		label: 'Email',
		regEx: SimpleSchema.RegEx.Email,
		autoform:{
			placeholder: 'Email'
		}
	},
	createdAt: {
		type:Date,
		label: 'createdAt',
		optional: true,
		autoValue: () => new Date(),
		autoform:{
			type:"hidden"
		}
	}
});

ClientsSchema.messageBox.messages({
	es:{
		required: '{{{label}}} es requerido.',
		minNumber: '{{{label}}} debe ser de {{{min.length}}} números.',
		maxNumber: '{{{label}}} no debe exceder {{{max.length}}} números.',
		expectedType: 'valor no compatible para {{{label}}}',
		regEx: '{{{label}}} no valido.'
	}
});

ClientsSchema.messageBox.setLanguage('es');

Clients.attachSchema(ClientsSchema);