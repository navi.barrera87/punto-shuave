import { Mongo } from 'meteor/mongo';
import SimpleSchema from 'simpl-schema';

import { MenuImages } from './menuimages.js';
import { MenuItems } from './menuitems.js';

SimpleSchema.extendOptions(['autoform']);

export const Menus = new Mongo.Collection('menus');

Menus.allow({
	insert: () => {
		return true;
	},
	update: () => {
		return true;
	},
	remove: () => {
		return true;
	}
});

Menus.helpers({
	images() { 
		return MenuImages.findOne({_id:this.image});
	},
	items() {
		let menuItems = MenuItems.find({menu:this._id}).fetch();
		for(menuItem of menuItems){
			if(menuItem.image!=undefined)
				menuItem.image = MenuItems.findOne({_id:menuItem._id}).images().fetch();
		}
		return menuItems;
	}
});

MenusSchema = new SimpleSchema({
	status:{
		type: String,
		label: 'Status',
		autoform:{
			options: ['Active','Inactive']
		}
	},
	name:{
		type: String,
		label: 'Name',
		autoform:{
			placeholder: 'Name'
		}
	},
	description:{
		type: String,
		label: 'Description',
		optional:true,
		autoform:{
			placeholder: 'Description'
		}
	},
	client:{
		type: String,
		label: 'Client',
		autoform:{
			placeholder: 'Client'
		}
	},
	image:{
		type: String,
		label: 'Menu Image',
		optional: true
	},
	createdAt: {
		type: Date,
		label: 'createdAt',
		optional: true,
		autoValue: () => new Date(),
		autoform:{
			type:"hidden"
		}
	}
});

MenusSchema.messageBox.messages({
	es:{
		required: '{{{label}}} es requerido.',
		expectedType: 'valor no compatible para {{{label}}}'
	}
});

MenusSchema.messageBox.setLanguage('es');

Menus.attachSchema(MenusSchema);