import { Mongo } from 'meteor/mongo';
import SimpleSchema from 'simpl-schema';

import { MenuItems } from './menuitems.js';

SimpleSchema.extendOptions(['autoform']);

export const OrderItems = new Mongo.Collection('orderitems');

OrderItems.allow({
	insert: () => {
		return true;
	},
	update: () => {
		return true;
	},
	remove: () => {
		return true;
	}
});

OrderItems.helpers({
	items() {
		return MenuItems.find({_id:this._menuitem});
	}
});

OrderItemsSchema = new SimpleSchema({
	status:{
		type: String,
		label: 'Status',
		autoform:{
			options: ['Ordered','Cooking','Ready']
		}
	},
	client: {
		type: String,
		label: 'Client',
		optional: true,
		autoform:{
			placeholder: 'Client'
		}
	},
	notes:{
		type: String,
		label: 'Notes',
		autoform:{
			placeholder: 'Notes'
		}
	},
	menuitem:{
		type: String,
		label: 'Menu Item',
		autoform:{
			type: 'hidden'
		}
	},
	order:{
		type: String,
		label: 'Order',
		autoform:{
			type: 'hidden'
		}
	},
	createdAt: {
		type: Date,
		label: 'createdAt',
		optional: true,
		autoValue: () => new Date(),
		autoform:{
			type:"hidden"
		}
	}
});

OrderItemsSchema.messageBox.messages({
	es:{
		required: '{{{label}}} es requerido.',
		expectedType: 'valor no compatible para {{{label}}}',
		minNumber: '{{{label}}} debe ser de 0 números.',
		maxNumber: '{{{label}}} no debe exceder 6 números.',
	}
});

OrderItemsSchema.messageBox.setLanguage('es');

OrderItems.attachSchema(OrderItemsSchema);