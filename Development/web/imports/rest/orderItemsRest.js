// Restful for Orders model
import { Router } from 'meteor/iron:router';

import { OrderItems } from '../api/orderitems.js';

Router.route('api/orderitems',{where: 'server'})
    .get( (req, res, next) => {
        res.setHeader('Content-Type','application/json');
        let orderItems = OrderItems.find().fetch();
        if(orderItems.length == 0){
            let response = {
                "error" : true,
                "message" : "No hay Items de Orden disponibles."
            };
            res.statusCode = 400;
            res.end(JSON.stringify(response));
        }
        res.statusCode = 200;
        res.end(JSON.stringify(orderItems));
    })
    .post( (req, res ,next) => {
        res.setHeader('Content-Type','application/json');
    	//postman remember to use x-www-form-urlencoded
    	let orderItem = validate(req,res);
        OrderItems.insert(orderItem);
        response = {
            "error" : false,
            "message" : "Item de Orden agregada."
        };
        res.statusCode = 200;
        res.end(JSON.stringify(response));
    }
);

Router.route('api/orderitems/:id',{where: 'server'})
    .get( (req, res, next) => {
        res.setHeader('Content-Type','application/json');
        let orderItem = findOrderItem(req,res);
        res.statusCode = 200;
        res.end(JSON.stringify(orderItem));
    })
    .put( (req, res, next) => {
        res.setHeader('Content-Type','application/json');
        let orderItemToUpdate = findOrderItem(req,res);
        let orderItem = validate(req,res);
        let updated = OrderItems.update({_id : orderItemToUpdate[0]._id},{$set: orderItem});
        if(updated != 1){
            response.message = 'Item de Orden no actualizada.';
            res.statusCode = 400;
            res.end(JSON.stringify(response));
        }
        response = {
            "error" : false,
            "message" : "Item de Orden actualizada."
        };
        res.statusCode = 200;
        res.end(JSON.stringify(response));
    })
    .delete( (req, res, next) => {
        res.setHeader('Content-Type','application/json');
        let orderItemToRemove = findOrderItem(req,res);
        let removed = OrderItems.remove(orderItemToRemove[0]._id);
        if(removed != 1){
            response.message = 'Item de Orden no pudo ser borrada.';
            res.statusCode = 400;
            res.end(JSON.stringify(response));
        }
        response = {
            "error" : false,
            "message" : "Item de Orden ha sido borrada."
        };
        res.statusCode = 200;
        res.end(JSON.stringify(response));
    }
);

Router.route('api/orderitems/:id/item',{where: 'server'})
    .get( (req, res, next) => {
        res.setHeader('Content-Type','application/json');
        let response = {
            "error" : true,
            "message" : "Item de Orden no encontrada."
        };
        let id = req.originalUrl.split('/')[3];
        if(id === undefined) {
            res.statusCode = 400;
            res.end(JSON.stringify(response));
            return false;
        }
        let orderItem = OrderItem.findOne({_id : id});
        orderItem.item = OrderItem.findOne({_id : id}).item();
        if(orderItem.length == 0) {
            res.statusCode = 400;
            res.end(JSON.stringify(response));
            return false;
        }
        res.statusCode = 200;
        res.end(JSON.stringify(orderItem));
    }
);

function findOrderItem (req, res) {
    let id = req.originalUrl.split('/')[3];
    let response = {
        "error" : true,
        "message" : "Item de Orden no encontrada."
    };
    if(id === undefined) {
        res.statusCode = 400;
        res.end(JSON.stringify(response));
        return false;
    }

    let orderItem = OrderItems.find({_id : id}).fetch();
    if(orderItem.length == 0) {
        res.statusCode = 400;
        res.end(JSON.stringify(response));
        return false;
    }
    return orderItem;
}

function validate (req, res) {
    let response;
    let orderItem = {
        status:req.body.status,
        client:req.body.client,
        notes:req.body.notes,
        menuitem: req.body.menuitem,
        order: req.body.order
    };

    let context = OrderItems.simpleSchema(orderItem).newContext();
    if(!context.validate(orderItem)) {
        let errors = context.validationErrors();
        let messages = {};

        for(err of errors)
            messages[err.name]= context.keyErrorMessage(err.name);

        response = {
            "error" : true,
            "messages" : messages
        };
        res.statusCode = 400;
        res.end(JSON.stringify(response));
        return false;
    }
    return orderItem;
}