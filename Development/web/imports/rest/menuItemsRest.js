// Restful for Menu Items model
import { Meteor } from 'meteor/meteor';
import { Router } from 'meteor/iron:router';

import { MenuItems } from '../api/menuitems.js';
import { MenuItemsImages } from '../api/menuitemsimages.js';

Router.route('api/menuitems',{where: 'server'})
    .get( (req, res, next) => {
        res.setHeader('Content-Type','application/json');
        let menuItems = MenuItems.find().fetch();
        for(menuItem of menuItems){
            if(menuItem.image!=undefined)
                menuItem.image = MenuItems.findOne({_id:menuItem._id}).images().fetch();
        }
        if(menuItems.length == 0){
            let response = {
                "error" : true,
                "messages" : "No se encuentran Items..."
            };
            res.statusCode = 400;
            res.end(JSON.stringify(response));
        }
        res.statusCode = 200;
        res.end(JSON.stringify(menuItems));
    })
    .post( (req, res ,next) => {
    	//postman remember to use x-www-form-urlencoded
        res.setHeader('Content-Type','application/json');
    	let menuItem = validate(req,res);
        if(req.filenames.length != 0){
            MenuItemsImages.addFile(req.filenames[0], (err, fileObj) => {
                if(err){
                    response = {
                        "error" : true,
                        "messages" : {image:err}
                    };
                    res.statusCode = 400;
                    res.end(JSON.stringify(response));
                    return false;
                }
                menuItem.image = fileObj._id;
                MenuItems.insert(menuItem);
                response = {
                    "error" : false,
                    "message" : "Item agregado."
                };
                res.statusCode = 200;
                res.end(JSON.stringify(response));
            });
            return false;
        }

        MenuItems.insert(menuItem);
        response = {
            "error" : false,
            "message" : "Item agregado."
        };
        res.statusCode = 200;
        res.end(JSON.stringify(response));
    }
);

Router.route('api/menuitems/:id',{where: 'server'})
    .get( (req, res, next) => {
        res.setHeader('Content-Type','application/json');
        let menuItem = findMenuItem(req,res);
        if(menuItem[0].image!=undefined)
            menuItem[0].image = MenuItems.findOne({_id : menuItem[0]._id}).images().fetch();
        if(menuItem.length == 0) {
            res.statusCode = 400;
            res.end(JSON.stringify(response));
            return false;
        }
        res.statusCode = 200;
        res.end(JSON.stringify(menuItem));
    })
    .post( (req, res, next) => {
        res.setHeader('Content-Type','application/json');
        let menuItemToUpdate = findMenuItem(req, res);
        let menuItem = validate(req,res);
        if(req.filenames.length != 0){
            MenuItemsImages.addFile(req.filenames[0], (err, fileObj) => {
                if(err){
                    response = {
                        "error" : true,
                        "messages" : {image:err}
                    };
                    res.statusCode = 400;
                    res.end(JSON.stringify(response));
                    return false;
                }
                menuItem.image = fileObj._id;
                let updated = MenuItems.update({_id : menuItemToUpdate[0]._id},{$set: menuItem});
                if(updated != 1){
                    response.message = 'Menu no actualizado.';
                    res.statusCode = 400;
                    res.end(JSON.stringify(response));
                    return false;
                }
                response = {
                    "error" : false,
                    "message" : "Menu actualizado."
                };
                res.statusCode = 200;
                res.end(JSON.stringify(response));
            });
            return false;
        }
        let updated = MenuItems.update({_id : menuItemToUpdate[0]._id},{$set: menuItem});
        if(updated != 1){
            response.message = 'Item no actualizado.';
            res.statusCode = 400;
            res.end(JSON.stringify(response));
        }
        response = {
            "error" : false,
            "message" : "Item actualizado."
        };
        res.statusCode = 200;
        res.end(JSON.stringify(response));
    })
    .delete( (req, res, next) => {
        res.setHeader('Content-Type','application/json');
        let menuItemToRemove = findMenuItem(req,res);
        let removed = MenuItems.remove(menuItemToRemove[0]._id);
        if(removed != 1){
            response.message = 'Item no pudo ser borrado.';
            res.statusCode = 400;
            res.end(JSON.stringify(response));
        }
        response = {
            "error" : false,
            "message" : "Menu Item ha sido borrado."
        };
        res.statusCode = 200;
        res.end(JSON.stringify(response));
    }
);

function findMenuItem (req, res) {
    let response = {
        "error" : true,
        "message" : "Menu Item no encontrado."
    };
    let id = req.originalUrl.split('/')[3];
    if(id === undefined) {
        res.statusCode = 400;
        res.end(JSON.stringify(response));
        return false;
    }
    let menuItem = MenuItems.find({_id : id}).fetch();
    if(menuItem.length == 0) {
        res.statusCode = 400;
        res.end(JSON.stringify(response));
        return false;
    }
    return menuItem;
}

function validate (req,res) {
    let response;
    let menuItem = {
        status: req.body.status,
        name: req.body.name,
        description: req.body.description,
        price: req.body.price*1,
        menu: req.body.menu
    };
    let context = MenuItems.simpleSchema(menuItem).newContext();
    if(!context.validate(menuItem)) {
        let errors = context.validationErrors();
        let messages = {};

        for(err of errors)
            messages[err.name]= context.keyErrorMessage(err.name);

        response = {
            "error" : true,
            "messages" : messages
        };
        res.statusCode = 400;
        res.end(JSON.stringify(response));
        return false;
    }
    return menuItem;
}