// Restful for Clients model
import { Router } from 'meteor/iron:router';

import { Clients } from '../api/clients.js';

Router.route('api/clients',{where: 'server'})
    .get( (req, res, next) => {
        res.setHeader('Content-Type','application/json');
        let clients = Clients.find().fetch();
        if(clients .length == 0){
            let response = {
                "error" : true,
                "message" : "No hay clientes disponibles..."
            };
            res.statusCode = 400;
            res.end(JSON.stringify(response));
        }
        res.statusCode = 200;
        res.end(JSON.stringify(clients));
    })
    .post( (req, res ,next) => {
        res.setHeader('Content-Type','application/json');
    	//postman remember to use x-www-form-urlencoded
    	let client = validate(req, res);
        Clients.insert(client);
        response = {
            "error" : false,
            "message" : "Cliente agregado."
        };
        res.statusCode = 200;
        res.end(JSON.stringify(response));
    }
);

Router.route('api/clients/:id',{where: 'server'})
    .get( (req, res, next) => {
        res.setHeader('Content-Type','application/json');
        let client = findClient(req,res);
        res.statusCode = 200;
        res.end(JSON.stringify(client));
    })
    .put( (req, res, next) => {
        res.setHeader('Content-Type','application/json');
        let clientToUpdate = findClient(req, res);
        let client = validate(req, res);
        let updated = Clients.update({_id : clientToUpdate[0]._id},{$set: client});
        if(updated != 1){
            response.message = 'Cliente no actualizado.';
            res.statusCode = 400;
            res.end(JSON.stringify(response));
        }
        response = {
            "error" : false,
            "message" : "Cliente actualizado."
        };
        res.statusCode = 200;
        res.end(JSON.stringify(response));
    })
    .delete( (req, res, next) => {
        res.setHeader('Content-Type','application/json');
        let clientToRemove = findClient(req, res);
        let removed = Clients.remove(clientToRemove[0]._id);
        if(removed != 1){
            response.message = 'Cliente no pudo ser borrado.';
            res.statusCode = 400;
            res.end(JSON.stringify(response));
        }
        response = {
            "error" : false,
            "message" : "Cliente ha sido borrado."
        };
        res.statusCode = 200;
        res.end(JSON.stringify(response));
    }
);

Router.route('api/clients/:id/menus',{where: 'server'})
    .get( (req, res, next) => {
        res.setHeader('Content-Type','application/json');
        let client = findClient(req,res);
        client[0].menus = Clients.findOne({_id:client[0]._id}).menus();
        res.statusCode = 200;
        res.end(JSON.stringify(client));
    }
);

function findClient (req,res) {
    let response = {
        "error" : true,
        "message" : "Cliente no encontrado."
    };
    let id = req.originalUrl.split('/')[3];
    if(id === undefined) {
        res.statusCode = 400;
        res.end(JSON.stringify(response));
        return false;
    }
    let client = Clients.find({_id : id}).fetch();
    if(client.length == 0) {
        res.statusCode = 400;
        res.end(JSON.stringify(response));
        return false;
    }
    return client;
}

function validate (req,res) {
    let response;
    let client = {
        name: req.body.name,
        l_name: req.body.l_name,
        zip: req.body.zip*1,
        status: req.body.status,
        dob: new Date(req.body.dob),
        address: req.body.address,
        city: req.body.city,
        state: req.body.state,
        phone: req.body.phone*1,
        email: req.body.email
    };
    let context = Clients.simpleSchema(client).newContext();
    if(!context.validate(client)) {
        let errors = context.validationErrors();
        let messages = {};

        for(err of errors)
            messages[err.name]= context.keyErrorMessage(err.name);

        response = {
            "error" : true,
            "messages" : messages
        };
        res.statusCode = 400;
        res.end(JSON.stringify(response));
        return false;
    }
    return client;
}