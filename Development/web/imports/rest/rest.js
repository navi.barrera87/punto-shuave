import { clientsRest } from './clientsRest.js';
import { menusRest } from './menusRest.js';
import { menuItemsRest } from './menuItemsRest.js';
import { ordersRest } from './ordersRest.js';
import { orderItemsRest } from './orderItemsRest.js';

    var fs = Npm.require("fs"),
        os = Npm.require("os"),
        path = Npm.require("path");

const folder = path.resolve('.').split(path.sep + '.meteor')[0]+'/assets/images/';

const posts = {
	'/api/menus':{allowed:true,folder:folder+'menus/'},
	'/api/menuitems':{allowed:true,folder:folder+'menus/items/'},
	'/api/clients':{allowed:false},
	'/api/orders':{allowed:false},
	'/api/orderitems':{allowed:false}
};

Router.onBeforeAction(function (req, res, next) {
    var filenames = []; // Store filenames and then pass them to request.
    let url = req.url;
    for(key in posts){
    	if(url.indexOf(key)>-1)
    		url = key;
    }
    if (req.method === "POST" && posts[url].allowed) {
        req.body = {};

      var busboy = new Busboy({ headers: req.headers });
      busboy.on("file", function (fieldname, file, filename, encoding, mimetype) {
        var saveTo = path.join(folder, filename);
        file.pipe(fs.createWriteStream(saveTo));
        filenames.push(saveTo);
      });
      busboy.on("field", function(fieldname, value) {
        req.body[fieldname] = value;
        //console.log(req.body);
      });
      busboy.on("finish", function () {
        // Pass filenames to request
        req.filenames = filenames;
        next();
      });
      // Pass request to busboy
      req.pipe(busboy);
      //next();
    }else next();
});