// Restful for Orders model
import { Router } from 'meteor/iron:router';

import { Orders } from '../api/orders.js';

Router.route('api/orders',{where: 'server'})
    .get( (req, res, next) => {
        res.setHeader('Content-Type','application/json');
        let orders = Orders.find().fetch();
        if(orders.length == 0){
            let response = {
                "error" : true,
                "message" : "No hay Ordenes disponibles."
            };
            res.statusCode = 400;
            res.end(JSON.stringify(response));
        }
        res.statusCode = 200;
        res.end(JSON.stringify(orders));
    })
    .post( (req, res ,next) => {
        res.setHeader('Content-Type','application/json');
    	//postman remember to use x-www-form-urlencoded
    	let order = validate(req,res);
        Orders.insert(order);
        response = {
            "error" : false,
            "message" : "Orden agregada."
        };
        res.statusCode = 200;
        res.end(JSON.stringify(response));
    }
);

Router.route('api/orders/:id',{where: 'server'})
    .get( (req, res, next) => {
        res.setHeader('Content-Type','application/json');
        let order = findOrder(req,res);
        res.statusCode = 200;
        res.end(JSON.stringify(order));
    })
    .put( (req, res, next) => {
        res.setHeader('Content-Type','application/json');
        let orderToUpdate = findOrder(req,res);
        let order = validate(req,res);
        let updated = Orders.update({_id : orderToUpdate[0]._id},{$set: order});
        if(updated != 1){
            response.message = 'Orden no actualizada.';
            res.statusCode = 400;
            res.end(JSON.stringify(response));
        }
        response = {
            "error" : false,
            "message" : "Orden actualizada."
        };
        res.statusCode = 200;
        res.end(JSON.stringify(response));
    })
    .delete( (req, res, next) => {
        res.setHeader('Content-Type','application/json');
        let orderToRemove = findOrder(req,res);
        let removed = Orders.remove(orderToRemove[0]._id);
        if(removed != 1){
            response.message = 'Orden no pudo ser borrada.';
            res.statusCode = 400;
            res.end(JSON.stringify(response));
        }
        response = {
            "error" : false,
            "message" : "Orden ha sido borrada."
        };
        res.statusCode = 200;
        res.end(JSON.stringify(response));
    }
);

Router.route('api/orders/:id/items',{where: 'server'})
    .get( (req, res, next) => {
        res.setHeader('Content-Type','application/json');
        let response = {
            "error" : true,
            "message" : "Orden no encontrada."
        };
        let id = req.originalUrl.split('/')[3];
        if(id === undefined) {
            res.statusCode = 400;
            res.end(JSON.stringify(response));
            return false;
        }
        let order = Orders.findOne({_id : id});
        order.items = Orders.findOne({_id : id}).items();
        if(order.length == 0) {
            res.statusCode = 400;
            res.end(JSON.stringify(response));
            return false;
        }
        res.statusCode = 200;
        res.end(JSON.stringify(order));
    }
);

function findOrder (req, res) {
    let id = req.originalUrl.split('/')[3];
    let response = {
        "error" : true,
        "message" : "Orden no encontrada."
    };
    if(id === undefined) {
        res.statusCode = 400;
        res.end(JSON.stringify(response));
        return false;
    }

    let order = Orders.find({_id : id}).fetch();
    if(order.length == 0) {
        res.statusCode = 400;
        res.end(JSON.stringify(response));
        return false;
    }
    return order;
}

function validate (req, res) {
    let response;
    let order = {
        status:req.body.status,
        date: new Date(req.body.date),
        subtotal:req.body.subtotal*1,
        total:req.body.total*1
    };

    let context = Orders.simpleSchema(order).newContext();
    if(!context.validate(order)) {
        let errors = context.validationErrors();
        let messages = {};

        for(err of errors)
            messages[err.name]= context.keyErrorMessage(err.name);

        response = {
            "error" : true,
            "messages" : messages
        };
        res.statusCode = 400;
        res.end(JSON.stringify(response));
        return false;
    }
    return order;
}