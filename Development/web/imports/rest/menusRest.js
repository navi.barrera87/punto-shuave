// Restful for Menus model
import { Meteor } from 'meteor/meteor';
import { Router } from 'meteor/iron:router';

import { Menus } from '../api/menus.js';
import { MenuImages } from '../api/menuimages.js';

Router.route('api/menus',{where: 'server'})
    .get( (req, res, next) => {
        res.setHeader('Content-Type','application/json');
        let menus = Menus.find().fetch();
        for(menu of menus){
            if(menu.image!=undefined)
                menu.image = Menus.findOne({_id:menu._id}).images().fetch();
        }
        if(menus.length == 0){
            let response = {
                "error" : true,
                "messages" : "Problemas con el servidor."
            };
            res.statusCode = 400;
            res.end(JSON.stringify(response));
        }
        res.statusCode = 200;
        res.end(JSON.stringify(menus));
    })
    .post( (req, res ,next) => {
    	//using busboy to get images and req.body
        res.setHeader('Content-Type','application/json');
        let menu = validate(req,res);
        if(req.filenames.length != 0){
            MenuImages.addFile(req.filenames[0], (err, fileObj) => {
                if(err){
                    response = {
                        "error" : true,
                        "messages" : {image:err}
                    };
                    res.statusCode = 400;
                    res.end(JSON.stringify(response));
                    return false;
                }
                menu.image = fileObj._id;
                Menus.insert(menu);
                response = {
                    "error" : false,
                    "message" : "Menu agregado."
                };
                res.statusCode = 200;
                res.end(JSON.stringify(response));
            });
            return false;
        }

        Menus.insert(menu);
        response = {
            "error" : false,
            "message" : "Menu agregado."
        };
        res.statusCode = 200;
        res.end(JSON.stringify(response));
    }
);

Router.route('api/menus/:id',{where: 'server'})
    .get( (req, res, next) => {
        res.setHeader('Content-Type','application/json');
        let menu = findMenu(req,res);
        if(menu[0].image!=undefined)
            menu[0].image = Menus.findOne({_id:menu[0]._id}).images().fetch();
        
        res.statusCode = 200;
        res.end(JSON.stringify(menu));
    })
    .post( (req, res, next) => { //sustitution of PUT cause of Busboy's lack of server methods
        res.setHeader('Content-Type','application/json');
        let menuToUpdate = findMenu(req, res);
        let menu = validate(req,res);
        if(req.filenames.length != 0){
            MenuImages.addFile(req.filenames[0], (err, fileObj) => {
                if(err){
                    response = {
                        "error" : true,
                        "messages" : {image:err}
                    };
                    res.statusCode = 400;
                    res.end(JSON.stringify(response));
                    return false;
                }
                menu.image = fileObj._id;
                let updated = Menus.update({_id : menuToUpdate[0]._id},{$set: menu});
                if(updated != 1){
                    response.message = 'Menu no actualizado.';
                    res.statusCode = 400;
                    res.end(JSON.stringify(response));
                    return false;
                }
                response = {
                    "error" : false,
                    "message" : "Menu actualizado."
                };
                res.statusCode = 200;
                res.end(JSON.stringify(response));
            });
            return false;
        }
        let updated = Menus.update({_id : menuToUpdate[0]._id},{$set: menu});
        if(updated != 1){
            response.message = 'Menu no actualizado.';
            res.statusCode = 400;
            res.end(JSON.stringify(response));
            return false;
        }
        response = {
            "error" : false,
            "message" : "Menu actualizado."
        };
        res.statusCode = 200;
        res.end(JSON.stringify(response));
    })
    .delete( (req, res, next) => {
        res.setHeader('Content-Type','application/json');
        let menuToRemove = findMenu(req, res);
        let removed = Menus.remove(menuToRemove[0]._id);
        if(removed != 1){
            response.message = 'Menu no pudo ser borrado.';
            res.statusCode = 400;
            res.end(JSON.stringify(response));
        }
        response = {
            "error" : false,
            "message" : "Menu ha sido borrado."
        };
        res.statusCode = 200;
        res.end(JSON.stringify(response));
    }
);

Router.route('api/menus/:id/items',{where: 'server'})
    .get( (req, res, next) => {
        res.setHeader('Content-Type','application/json');
        let response = {
            "error" : true,
            "message" : "Menu no encontrado."
        };
        let id = req.originalUrl.split('/')[3];
        if(id === undefined) {
            res.statusCode = 400;
            res.end(JSON.stringify(response));
            return false;
        }

        let menu = Menus.findOne({_id : id});
        if(menu.image!=undefined)
            menu.image = Menus.findOne({_id : id}).images().fetch();
        menu.items = Menus.findOne({_id : id}).items();
        if(menu.length == 0) {
            res.statusCode = 400;
            res.end(JSON.stringify(response));
            return false;
        }

        res.statusCode = 200;
        res.end(JSON.stringify(menu));
    }
);

function findMenu (req,res) {
    let response = {
        "error" : true,
        "message" : "Menu no encontrado."
    };
    let id = req.originalUrl.split('/')[3];
    if(id === undefined) {
        res.statusCode = 400;
        res.end(JSON.stringify(response));
        return false;
    }
    let menu = Menus.find({_id : id}).fetch();
    if(menu.length == 0) {
        res.statusCode = 400;
        res.end(JSON.stringify(response));
        return false;
    }
    return menu;
}

function validate (req,res) {
    let response;
    let menu = {
        status: req.body.status,
        name: req.body.name,
        description: req.body.description,
        client: req.body.client
    };
    let context = Menus.simpleSchema(menu).newContext();
    if(!context.validate(menu)) {
        let errors = context.validationErrors();
        let messages = {};

        for(err of errors)
            messages[err.name]= context.keyErrorMessage(err.name);

        response = {
            "error" : true,
            "messages" : messages
        };
        res.statusCode = 400;
        res.end(JSON.stringify(response));
        return false;
    }
    return menu;
}