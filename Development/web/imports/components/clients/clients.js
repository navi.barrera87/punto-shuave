import angular from 'angular';
import angularMeteor from 'angular-meteor';

import { Clients } from '../../api/clients.js';
import { Menus } from '../../api/menus.js';

import template from './clients.html';
 
class ClientsCtrl {
  constructor($scope) {
    $scope.viewModel(this);
 
    this.helpers({
      clients() {
        return Clients.find({});
      },
      menus() {
        return Menus.find();
      }
    })
  }
}
 
export default angular.module('clients', [
  angularMeteor
])
  .component('clients', {
    templateUrl: 'imports/components/clients/clients.html',
    controller: ['$scope',ClientsCtrl]
  });